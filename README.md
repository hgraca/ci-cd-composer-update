# **CI/CD Composer Update**

[![Author](http://img.shields.io/badge/author-@hgraca-blue.svg)](https://www.herbertograca.com)
[![Software License](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/hgraca/ci-cd-composer-update?label=version&sort=semver&color=blue)](https://gitlab.com/hgraca/ci-cd-composer-update/-/tags)

A library of CI/CD recipes to run `composer update`, open an MR and merge it if the pipeline is green.

Currently, only GitLab is supported, but when the need arises, more CI/CD providers will be added.

If you have need for another CI/CD provider, feel free to open an MR.

## **Why is this needed?**

There are two very good tools for dependency updates:
- [RenovateBot](https://github.com/renovatebot/renovate)
- [DependaBot](https://github.com/dependabot/dependabot-core)

Unfortunately they don't work well with PHP, they provide a lot of funcitonality (i.e. MR for each package that
needs to be updated) and for that they need a lot of information, so they try to replicate everything
that composer does, but without complete success.

For example, RenovateBot will only work well if you are running your project with the latest version of PHP, as
soon as a new version is released and packages are changed to comply to it, it will install packages that
work in the new version of PHP but not in your older version.

This is because it doesn't factor in the PHP version requirements of all depedencies when figuring out what
package versions to install.

## **How to use**

First import this library into your project:

`composer require --dev hgraca/ci-cd-composer-update`

### **GitLab**

#### **1. Include this package in your GitLab config**
This package defines a GitLab yaml file, which you need to include in your projects' GitLab yaml config, for example:

```yaml
# .gitlab-ci.yml
include:
  - remote: 'https://gitlab.com/hgraca/ci-cd-composer-update/raw/v1.0.0/src/GitLab/update_PHP_deps.yml'
```
The include type must be `remote`, because when your project pipeline starts, `composer install` has not been run yet,
thus it will fail to find a local file to include.

This file provides a disabled (aka "abstract") job named `.update_PHP_deps`.

#### **2. Create your update job**
Then you can create your update job, extending the one provided by this package (`.update_PHP_deps`),
provide a few variables, the stage in which it should run and possibly a `before_script`
to install the dependencies (`git`, `jq` and `curl`).

The access token to be used in`GITLAB_ACCESS_TOKEN` **MUST** be either a personal access token or a group access token. **A project access token will not work** as it will lack permissions for the API and docker registry.

For example:

```yaml
update_PHP_deps:
  extends: .update_PHP_deps
  variables:
    UPDATE_BRANCH_PREFIX: "update_PHP_deps_" # Used for the update branch name, it will be followed by the datetime
    GIT_USER: "Maintainer Bot" # Used for the update commit
    GIT_EMAIL: "maintainer.bot@gmail.com" # Used for the update commit
    GITLAB_USERNAME: "my_username" # Used for pushing the new branch and opening the MR
    GITLAB_ACCESS_TOKEN: "personal access token with 'api' and 'write repository' permissions" # Used for pushing the new branch and opening the MR
    MERGE_IF_SUCCESSFUL: "true" # Merge automatically if the pipeline succeeds
    SECONDS_BETWEEN_POOLING: 10 # Nbr of seconds between checking if the MR pipeline is successful, so then it will merge
  stage: update_dependencies
  before_script: # `git`, `jq` and `curl` are needed, so if they are not already installed, they should be installed here
    - !reference [ default, before_script ] # Include any other `before_script` that might be defined
    - apk add --no-cache --update git jq curl && rm -rf /var/cache/apk/*
    - git config --global user.name "${GIT_USER}"
    - git config --global user.email "${GIT_EMAIL}"
```

Check all the variables you can override or ignore by looking in `src/GitLab/update_PHP_deps.yml`.

This job will not run in your pipelines because it has the rule that it only runs if
there is a variable named `SCHEDULED_PIPELINE` with your job name as value.
This will make sure the job will only run on a scheduled pipeline.

**2.1 Custom GIT, CURL and Composer flags**

If you need to set custom flags for git or curl operations, you can use the variables `JOB_GIT_FLAGS` and `JOB_CURL_FLAGS`.

For example, if you are on a self hosted gitlab instance, you might have issues with self signed certificates.

Similarly, you might want to tell composer to ignore specific platform requirements.

To circumvent this, you can do:

```yaml
update_PHP_deps:
  extends: .update_PHP_deps
  variables:
    # ...
    JOB_GIT_FLAGS: "-c http.sslVerify=false"
    JOB_CURL_FLAGS: "--insecure"
    JOB_COMPOSER_FLAGS: "--ignore-platform-req=ext-bcmath --ignore-platform-req=ext-intl --ignore-platform-req=ext-soap"
  # ...
```

#### **3. Schedule a pipeline in the GitLab UI**
Now you can create the pipeline schedule to run your update job regularly.
You can set it up in any way you want, you just need to make sure that you configure
a variable named `SCHEDULED_PIPELINE` with your job name as value.

Also, note that the branch you choose in the `Edit Pipeline Schedule` screen, is the branch that will be the
target of the update. The branch with the update will be made from this branch and the MR will be merged
into this branch.

Following the example job above, you would need to set a variable with key `SCHEDULED_PIPELINE`
and value `update_PHP_deps`.

For example:

![Schedule the update pipeline](docs/images/gitlab/edit_pipeline_schedule.png)
