#!/usr/bin/env sh

ACCESS_TOKEN=""
CURL_FLAGS=""
GITLAB_API="${CI_API_V4_URL}"
MERGE_IF_SUCCESSFUL="true"
MR_BRANCH=""
MR_TITLE="Auto-Merge-$(date '+%Y%m%d%H%M%S')"
PROJECT_ID="${CI_PROJECT_ID}"
SECONDS_BETWEEN_POOLING=10
TARGET_BRANCH="main"
eval "${@}"

if [ "${MR_BRANCH}" = '' ]; then
  echo "[ERROR] No MR branch was given."
  exit 1
fi

if [ "${ACCESS_TOKEN}" = '' ]; then
  echo "[ERROR] No access token was given."
  exit 2
fi

if [ "${MERGE_IF_SUCCESSFUL}" = "1" ] || [ "${MERGE_IF_SUCCESSFUL}" = "true" ]; then
  MERGE_IF_SUCCESSFUL="true"
else
  MERGE_IF_SUCCESSFUL="false"
fi

MR_JSON=$(curl ${CURL_FLAGS} \
  --request POST \
  --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
  --data-urlencode "source_branch=${MR_BRANCH}" \
  --data-urlencode "target_branch=${TARGET_BRANCH}" \
  --data-urlencode "title=${MR_TITLE}" \
  --data-urlencode "remove_source_branch=true" \
  "${GITLAB_API}/projects/${PROJECT_ID}/merge_requests"
)
CURL_EXIT_CODE=$?

if [ "${CURL_EXIT_CODE}" != "0" ]; then
  echo "[ERROR] Please check the returned response for details."
  echo "[INFO] CURL_EXIT_CODE=${CURL_EXIT_CODE}"
  echo "[INFO] MR_JSON=${MR_JSON}"
  exit ${CURL_EXIT_CODE}
fi

echo "[SUCCESS] Merge request created successfully!"
MR_URL=$(echo "${MR_JSON}" | jq '.web_url')
echo "[INFO] Merge request URL: ${MR_URL}"

if [ "${MERGE_IF_SUCCESSFUL}" = "true" ]; then
  MR_ID=$(echo "${MR_JSON}" | jq '.iid')

  echo "[INFO] Will merge MR ID ${MR_ID} (branch '${MR_BRANCH}' into '${TARGET_BRANCH}'), if and when the pipeline passes."
  echo "[INFO] Waiting for pipeline to finish..."
  while true; do
    PIPELINE_STATUS=$(curl ${CURL_FLAGS} --silent --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
      "${GITLAB_API}/projects/${PROJECT_ID}/merge_requests/${MR_ID}/pipelines" | jq '.[0].status')

    if [ "${PIPELINE_STATUS}" = "\"success\"" ]; then
      echo "[INFO] Pipeline succeeded!"
      # Merge the merge request
      curl ${CURL_FLAGS} --request PUT --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
        "${GITLAB_API}/projects/${PROJECT_ID}/merge_requests/${MR_ID}/merge"
      echo "[SUCCESS] Merge request merged successfully!"
      exit 0
    elif [ "${PIPELINE_STATUS}" = "\"failed\"" ]; then
      echo "[ERROR] Pipeline failed!"
      exit 1
    else
      echo "[INFO] Pipeline still running..."
      sleep ${SECONDS_BETWEEN_POOLING}
    fi
  done
fi
